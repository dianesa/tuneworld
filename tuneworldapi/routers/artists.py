from fastapi import APIRouter, Response, Depends, HTTPException
from queries.artists import ArtistIn, ArtistOut, Error, ArtistRepository
from typing import Union, List
from authenticator import authenticator

router = APIRouter()


@router.post("/artists", response_model=Union[ArtistOut, Error])
def create_artist(
    artist: ArtistIn,
    response: Response,
    repo: ArtistRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        artist.account_id = account_data["id"]
        artist = repo.create(artist)
    except Exception:
        return {"message": "Could not create Artist"}
    return artist


@router.get("/artists/{name}", response_model=Union[ArtistOut, Error])
def get_artist(
    name: str,
    repo: ArtistRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> ArtistOut:
    artist = repo.get(name)
    if artist is None:
        raise HTTPException(status_code=404, detail="Artist not found")
    return artist


@router.delete("/artists/{uri}", response_model=Union[bool, Error])
def delete_artist(
    uri: str,
    repo: ArtistRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    account_id = account_data["id"]
    return repo.delete(uri, account_id)


@router.get("/artists", response_model=List[ArtistOut])
def get_all(
    repo: ArtistRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.get_all(account_id)

from fastapi import APIRouter, Response, Depends, HTTPException
from queries.songs import SongIn, SongOut, Error, SongRepository
from typing import Union, List
from authenticator import authenticator

router = APIRouter()


@router.post("/songs", response_model=Union[SongOut, Error])
def create_song(
    song: SongIn,
    response: Response,
    repo: SongRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        song.account_id = account_data["id"]
        song = repo.create(song)
    except Exception:
        return {"message": "Could not create song"}
    return song


# May need to change title to ID
@router.get("/songs/{title}", response_model=Union[SongOut, Error])
def get_song(
    title: str,
    repo: SongRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> SongOut:
    song = repo.get(title)
    if song is None:
        raise HTTPException(status_code=404, detail="Item not found")
    return song


@router.delete("/songs/{uri}/", response_model=Union[bool, Error])
def delete_song(
    uri: str,
    repo: SongRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    account_id = account_data["id"]
    return repo.delete(uri, account_id)


@router.get("/songs", response_model=List[SongOut])
def get_all(
    repo: SongRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.get_all(account_id)

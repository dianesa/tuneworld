# Test client
from fastapi.testclient import TestClient
from queries.albums import AlbumRepository, AlbumIn, AlbumOut
from authenticator import authenticator
from main import app
import json

# Instantiating client
client = TestClient(app=app)


def get_current_account_data_mock():
    return {"id": 10}


class AlbumRepositoryMock:
    def create(self, album: AlbumIn) -> AlbumOut:
        album_dict = album.dict()
        return AlbumOut(id=2, **album_dict)


def test_create_album():
    # Arrange
    app.dependency_overrides[AlbumRepository] = AlbumRepositoryMock
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_account_data_mock
    album_body = {
        "name": "Petals for Armor",
        "uri": "4HXpQ5KQBVWN25ltjnX7xa",
        "picture_url":
        "https://i.scdn.co/image/ab67616d0000b273896e2483613a566bcb00d324",
        "artist": "Hayley Williams",
        "account_id": 1,
    }
    # Act
    response = client.post("/albums", json.dumps(album_body))

    # Assert
    assert response.status_code == 200
    assert response.json()["id"] == 2
    assert response.json()["account_id"] == 10

    # A cleanup
    app.dependency_overrides = {}

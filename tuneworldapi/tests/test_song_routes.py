# Test client
from fastapi.testclient import TestClient
from queries.songs import SongRepository, SongIn, SongOut
from authenticator import authenticator
from typing import List
from main import app
import json

# Instantiating client
client = TestClient(app=app)


def get_current_account_data_mock():
    return {"id": 5}


class SongRepositoryMock:
    def create(self, song: SongIn) -> SongOut:
        song_dict = song.dict()
        return SongOut(id=2, **song_dict)

    def get_all(self, title: str) -> List[SongOut]:
        return []


def test_create_song():
    # Arrange
    app.dependency_overrides[SongRepository] = SongRepositoryMock
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_account_data_mock
    song_body = {
        "uri": "string",
        "title": "string",
        "artist": "string",
        "time": 0,
        "album": "string",
        "preview_url": "string",
        "account_id": 30,
    }
    # Act
    response = client.post("/songs", json.dumps(song_body))

    # Assert
    assert response.status_code == 200
    assert response.json()["id"] == 2
    assert response.json()["account_id"] == 5

    # A cleanup
    app.dependency_overrides = {}


def test_get_all():
    app.dependency_overrides[SongRepository] = SongRepositoryMock
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = get_current_account_data_mock

    response = client.get("/songs")

    assert response.status_code == 200
    assert response.json() == []

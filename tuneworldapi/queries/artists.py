from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional


class Error(BaseModel):
    message: str


class ArtistIn(BaseModel):
    name: str
    uri: str
    picture_url: str
    account_id: Optional[int]


class ArtistOut(ArtistIn):
    id: int


class ArtistRepository:
    def create(self, artist: ArtistIn):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO artists
                            (name, uri, picture_url, account_id)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            artist.name,
                            artist.uri,
                            artist.picture_url,
                            artist.account_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = artist.dict()
                    return ArtistOut(id=id, **old_data)
        except Exception:
            return {"message": "Could not add artist"}

    def get(self, name: str) -> ArtistOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM artists
                        WHERE name = %s
                        """,
                        [name],
                    )
                    for row in db.fetchall():
                        record = {}
                        for i, column in enumerate(db.description):
                            record[column.name] = row[i]
                        return record
        except Exception as e:
            print(e)
            return {"message": "Could not get the artist"}

    def delete(self, uri: str, account_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM artists
                        WHERE uri = %s AND account_id = %s
                        """,
                        [uri, account_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_all(self, account_id: int) -> List[ArtistOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name, uri, picture_url, account_id
                        FROM artists
                        WHERE account_id = %s
                        ORDER BY name;
                        """,
                        [account_id],
                    )
                    lst = []
                    for record in db:
                        artist = ArtistOut(
                            id=record[0],
                            name=record[1],
                            uri=record[2],
                            picture_url=record[3],
                            account_id=record[4],
                        )
                        lst.append(artist)
                    return lst
        except Exception:
            return {"message": "Could not get all artists"}

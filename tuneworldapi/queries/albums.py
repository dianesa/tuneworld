from pydantic import BaseModel
from queries.pool import pool
from typing import List, Optional


class Error(BaseModel):
    message: str


class AlbumIn(BaseModel):
    name: str
    uri: str
    picture_url: str
    artist: str
    account_id: Optional[int]


class AlbumOut(AlbumIn):
    id: int


class AlbumRepository:
    def create(self, album: AlbumIn):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO albums
                            (name, uri, picture_url, artist, account_id)
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            album.name,
                            album.uri,
                            album.picture_url,
                            album.artist,
                            album.account_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    old_data = album.dict()
                    return AlbumOut(id=id, **old_data)
        except Exception:
            return {"message": "Could not add album"}

    def get(self, name: str) -> AlbumOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT *
                        FROM albums
                        WHERE name = %s
                        """,
                        [name],
                    )
                    for row in db.fetchall():
                        record = {}
                        for i, column in enumerate(db.description):
                            record[column.name] = row[i]
                        return record
        except Exception as e:
            print(e)
            return {"message": "Could not get the album"}

    def delete(self, uri: str, account_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM albums
                        WHERE uri = %s AND account_id = %s
                        """,
                        [uri, account_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_all(self, account_id: int) -> List[AlbumOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name, uri, picture_url, artist, account_id
                        FROM albums
                        WHERE account_id = %s
                        ORDER BY name;
                        """,
                        [account_id],
                    )
                    lst = []
                    for record in db:
                        album = AlbumOut(
                            id=record[0],
                            name=record[1],
                            uri=record[2],
                            picture_url=record[3],
                            artist=record[4],
                            account_id=record[5],
                        )
                        lst.append(album)
                    return lst
        except Exception:
            return {"message": "Could not get all albums"}

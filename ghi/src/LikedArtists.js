import React, { useState, useEffect } from "react";
import "./Artist/ArtistProfile.css";
import { useAuthContext } from "./useToken";

function LikedArtists() {
  const [artists, setArtists] = useState([]);
  const { token } = useAuthContext();

  useEffect(() => {
    async function fetchData() {
      try {
        await new Promise((r) => setTimeout(r, 100));
        const response = await fetch(
          `${process.env.REACT_APP_TUNEWORLD_API_HOST}/artists`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        const data = await response.json();
        setArtists(data);
      } catch (error) {
        console.log(error);
      }
    }
    if (token) {
      fetchData();
    }
  }, [token]);

  return (
    <>
      <div className="subGrid">
        {artists?.map((artist, i) => {
          return (
            <div key={i}>
              <div
                id={artist.uri}
                onClick={() =>
                  (window.location.href = `https://open.spotify.com/artist/${
                    artist.uri.slice(0, 15) === "spotify:artist:"
                      ? artist.uri.slice(15)
                      : artist.uri
                  }`)
                }
              >
                <div key={artist.id} className="albumCard">
                  <div className="albumContainer" align="center">
                    <div align="center">
                      <img src={artist.picture_url} alt="Artist" />
                    </div>
                    <h4 align="center">
                      <b>{artist.name}</b>
                    </h4>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}
export default LikedArtists;

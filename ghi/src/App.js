import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";

import Login from "./LogInForm";
import SignUp from "./SignUpForm";

import { AuthProvider, useToken } from "./useToken";
import Nav from "./Nav";

import ArtistText from "./Artist/Bio";
import ProfileNav from "./Artist/ProfileNav";
import MyFavorites from "./Artist/Favorites";
import UpdateProfile from "./Artist/UpdateProfile";
import ExternalLinks from "./Artist/ExternalLinks";

import Search from "./Search";

function GetToken() {
  // Get token from JWT cookie (if already logged in)
  useToken();
  return null;
}
const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, '');

function App() {
  return (
    <>
      <BrowserRouter basename={basename}>
        <AuthProvider>
          <GetToken />
          <Nav />
          <div className="parentGrid">
            {/* link column sticky left */}

            <div className="sidenav">
              <Routes>
                <Route path="/profile" element={<ProfileNav />} />
                <Route path="/profile/edit" element={<ProfileNav />} />
                <Route path="/profile/favorites" element={<ProfileNav />} />
              </Routes>
            </div>

            {/* Scrollable center component */}

            <div className="centerColumn">
              <Routes>
                <Route path="/" element={<Search />} />
                <Route path="/login" element={<Login />} />
                <Route path="/signup" element={<SignUp />} />
                <Route path="/profile" element={<ArtistText />} />
                <Route path="/profile/favorites" element={<MyFavorites />} />
                <Route path="/profile/edit" element={<UpdateProfile />} />
              </Routes>
            </div>

            {/* right sticky social media link bar */}

            <div className="rightSidebar">
              <Routes>
                <Route path="/profile" element={<ExternalLinks />} />
                <Route path="/profile/favorites" element={<ExternalLinks />} />
              </Routes>
            </div>
          </div>
        </AuthProvider>
      </BrowserRouter>
    </>
  );
}

export default App;

import { useState, useEffect } from "react";
import LikedArtists from "../LikedArtists";
import LikedAlbums from "../LikedAlbums";
import LikedSongs from "../LikedSongs";
import { useAuthContext } from "../useToken";
import "./ArtistProfile.css";
import "./Favorites.css";

let initialState = {
  artist: "",
  artistHeading: "stickyActive",
  album: "hidden",
  albumHeading: "stickyHeading",
  song: "hidden",
  songHeading: "stickyHeading",
};

function MyFavorites() {
  const { token } = useAuthContext();

  useEffect( () => {
      async function getAccount(){
      try {
        const url = `${process.env.REACT_APP_TUNEWORLD_API_HOST}/api/accounts/id`;
        const response = await fetch(url, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`
          },
          credentials:"include"
        });
        await response.json();
      } catch (error) {
        console.log(error);
      }}
      if(token){
        getAccount()
      }
    },[token])

  const [visibility, setVisibility] = useState(initialState);
  return (
    <>
      <div>
        <br></br>
        <div className="headingGrid">
          <div
            onClick={() => {
              setVisibility({
                artist: "",
                artistHeading: "stickyActive",
                album: "hidden",
                albumHeading: "stickyHeading",
                song: "hidden",
                songHeading: "stickyHeading",
              });
            }}
            className={visibility.artistHeading}
          >
            <h3>
              <b>Artists</b>
            </h3>
          </div>
          <div
            onClick={() => {
              setVisibility({
                artist: "hidden",
                artistHeading: "stickyHeading",
                album: "",
                albumHeading: "stickyActive",
                song: "hidden",
                songHeading: "stickyHeading",
              });
            }}
            className={visibility.albumHeading}
          >
            <h3>
              <b>Albums</b>
            </h3>
          </div>
          <div
            onClick={() => {
              setVisibility({
                artist: "hidden",
                artistHeading: "stickyHeading",
                album: "hidden",
                albumHeading: "stickyHeading",
                song: "",
                songHeading: "stickyActive",
              });
            }}
            className={visibility.songHeading}
          >
            <h3>
              <b>Songs</b>
            </h3>
          </div>
        </div>
        <div id="Artist" className={visibility.artist}>
          <LikedArtists />
        </div>
        <div className={visibility.album}>
          <LikedAlbums />
        </div>
        <div id="Song" className={visibility.song}>
          <LikedSongs />
        </div>
      </div>
    </>
  );
}

export default MyFavorites;

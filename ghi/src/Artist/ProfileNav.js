import "./ArtistProfile.css";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import { useAuthContext } from "../useToken";

let initialState = {
  profile: "profileButton",
  favorites: "",
};

function ProfileNav() {
  const [visibility, setVisibility] = useState(initialState);
  const { token } = useAuthContext();
  const [currAccount, setCurrAccount] = useState("");
  const navigate = useNavigate();

    useEffect( () => {
      async function getAccount(){
      try {
        const url = `${process.env.REACT_APP_TUNEWORLD_API_HOST}/api/accounts/id`;
        const response = await fetch(url, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`
          },
          credentials:"include"
        });
        const data = await response.json();
        if (response.ok){
          setCurrAccount(data)
      }
      } catch (error) {
        console.log(error);
      }}
      if(token){
        getAccount()
      }
    },[token])


  function toProfile() {
    setVisibility({ profile: "profileButton", favorites: "" });
    navigate("/profile");
  }
  function toFavorites() {
    setVisibility({ profile: "", favorites: "favoritesButton" });
    navigate("/profile/favorites");
  }
  function editProfile() {
    navigate("/profile/edit");
  }

  return (
    <>
      <div className="profileContainer">
        <br></br>
        <div className="avatar">
          <img className="profilePhoto" src={currAccount.picture_url} alt="PFP"></img>
          <h3 className="profileName">{currAccount.full_name}</h3>
          <p className="updateProfile" onClick={editProfile}>
            (edit my profile)
          </p>
          <hr style={{ color: "white" }}></hr>
        </div>
        <ul className="profileItems">
          <li id={visibility.profile} onClick={toProfile}>
            <b>My Profile</b>
          </li>
          <li id={visibility.favorites} onClick={toFavorites}>
            <b>My Favorites</b>
          </li>
        </ul>
      </div>
    </>
  );
}

export default ProfileNav;

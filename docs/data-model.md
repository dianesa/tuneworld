## Data Models

# Account

| name        | type        | unique      | optional    |
| ----------- | ----------- | ----------- | ----------- |
| email       | string      | yes         | no          |
| password    | string      | no          | no          |
| full_name   | string      | no          | no          |
| bio         | string      | no          | yes         |
| picture_url | string      | no          | yes         |


# Token

| name           | type        | unique      | optional    |
| -------------- | ----------- | ----------- | ----------- |
| username(email)| string      | yes         | no          |
| password       | string      | no          | no          |


# Artist

| name        | type        | unique      | optional    |
| ----------- | ----------- | ----------- | ----------- |
| name        | string      | no          | no          |
| uri         | string      | no          | no          |
| picture_url | string      | no          | yes         |
| account_id  | int         | no          | no          |


# Albums

| name        | type        | unique      | optional    |
| ----------- | ----------- | ----------- | ----------- |
| name        | string      | no          | no          |
| uri         | string      | no          | no          |
| picture_url | string      | no          | yes         |
| artist      | string      | no          | yes         |
| account_id  | int         | no          | no          |

# Songs

| name        | type        | unique      | optional    |
| ----------- | ----------- | ----------- | ----------- |
| uri         | string      | no          | no          |
| title       | string      | no          | no          |
| time        | int         | no          | no          |
| artist      | string      | no          | yes         |
| album       | string      | no          | no          |
| preview_url | string      | no          | yes         |
| account_id  | int         | no          | no          |
